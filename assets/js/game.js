//Snake
Crafty.modules({
    Joystick: "release"
}, function() {
    Crafty.init(480, 480).canvas.init();
    Crafty.background("black");
    //
/*
    var stick = Crafty.e("2D, DOM, Image").attr({
        x: 55,
        y: 55,
        w: 50,
        h: 50,
        z: 1
    }).image("http://cdn1.iconfinder.com/data/icons/function_icon_set/circle_green.png");

    var track = Crafty.e("2D, DOM, Image, Joystick, Text").attr({
        x: 50,
        y: 50,
        w: 64,
        h: 64
    }).image("http://cdn1.iconfinder.com/data/icons/softwaredemo/PNG/64x64/Circle_Grey.png").joystick(stick, {
        mouseSupport: true,
        range: 40
    });
    */
    //
    Crafty.c("Crossway", {
        init: function() {
            this.requires("Fourway");
            this._keydown = this.__keydown;
            this._keyup = this.__keyup;
        },
        crossway: function(speed) {
            return this.fourway(speed);
        },
        __keydown: function(e) {
            if (this._keys[e.key]) {
                this._movement.x = this._keys[e.key].x;
                this._movement.y = this._keys[e.key].y;
                this._lastkey = e.key;
                this.trigger('NewDirection', this._movement);
            }
        },

        __keyup: function(e) {
            if (this._keys[e.key] && e.key === this._lastkey) {
                this._movement.x = 0;
                this._movement.y = 0;
                this.trigger('NewDirection', this._movement);
            }
        }
    });
    //
    Crafty.c("Player", {
        init: function() {
            this.requires("Crossway");
        },
        player: function(blocksize) {
            return this.crossway(blocksize);
        }
    });
    Crafty.c("RainbowColor", {
        init: function() {
            this.requires('Color');
        },
        rainbowcolor: function() {
            var c = ["red", "green", "blue", "white"];
            var i = Crafty.math.randomInt(0, 3);
            this.color(c[i]);
            return this;
        }
    });
    Crafty.c("SnakeHead", {
        init: function() {},
        snakehead: function(blocksize, snake) {
            this._blocksize = blocksize;
            this.Snake = snake;
            this.bind("NewDirection", function(xy) {
                this.CurrentDirection = xy;
                this.Snake.CurrentDirection = xy;
                console.log("pp", this.Snake.Parts.length);
            }).bind("Move", function() {
                this.Snake.x = this.x;
                this.Snake.y = this.y;
                var pp = this.Snake.Parts;
                for (var i = pp.length - 1; i > 0; i--) {
                    var p = pp[i];

                    if (p.CurrentDirection) {
                        p.x += p.CurrentDirection.x;
                        p.y += p.CurrentDirection.y;
                    }

                    if (p.PreviousSnakePart) {
                        var ld = p.PreviousSnakePart.CurrentDirection;
                        p.CurrentDirection = {
                            x: ld.x,
                            y: ld.y
                        };
                    }
                }
            });
        }
    });
    Crafty.c("SnakePart", {
        init: function() {
            this.requires('2D, Canvas, RainbowColor');
        },

        snakepart: function(blocksize, x, y, snake, direction, partindex) {
            this.Snake = snake;
            this.PartIndex = partindex;
            this.CurrentDirection = direction;
            this.rainbowcolor().attr({
                w: blocksize,
                h: blocksize,
                x: x,
                y: y
            });
            return this;
        }
    });
    Crafty.c("Flower", {
        init: function() {
            this.requires('2D, Canvas, Color, Collision');
        },
        flower: function(blocksize, x, y) {
            this.color("yellow").attr({
                w: blocksize*3,
                h: blocksize*3,
                x: x,
                y: y
            });
            return this;
        }
    });
    Crafty.c("Snake", {
        init: function() {},
        snake: function(blocksize, x, y) {
            this.CurrentDirection = {x:-blocksize,y:0};
            this._blocksize = blocksize;
            this.Parts = pp = [];
            this.x = x;
            this.y = y;
            var defaultpartcount = 4;
            for (var i = 0; i < defaultpartcount; i++) {
                this.AddNewPart();
                x += blocksize;
            }
            pp[0].addComponent("SnakeHead").snakehead(blocksize, this);
            return this;
        },
        AddNewPart: function(color) {
            var pp = this.Parts;
            var partindex = this.Parts.length;
            previoussnakepart = pp[partindex - 1] || this;
            var previousdirection = previoussnakepart.CurrentDirection;
            var previousx = previoussnakepart.x;
            var previousy = previoussnakepart.y;

            var x = previousx + -previousdirection.x;
            var y = previousy + -previousdirection.y;

            var p = Crafty.e("SnakePart").snakepart(this._blocksize, x, y, this, previousdirection, partindex);
            if (color) {
                p.color(color);
            }
            p.PreviousSnakePart = previoussnakepart;

            pp.push(p);
            return p;
        }
    });
    Crafty.c("Floor", {
        init: function() {},
        createFlower: function() {

            var x = Crafty.math.randomInt(0, 450);
            var y = Crafty.math.randomInt(0, 450);
            var f = Crafty.e("Flower").flower(this.blocksize, x, y).onHit("SnakePart", function(oo) {
                var o = oo[0].obj;
                o.Snake.AddNewPart(this.color());
                this.destroy();
                this.World.createFlower();
            });
            f.World = this;
        },
        floor: function(blocksize) {
            this.blocksize = blocksize;
            this.createFlower();
        }
    });
    Crafty.c("SnakeGame", {
        init: function() {},
        snakegame: function(blocksize) {
            console.log("SnakeGame");
            this.Snake = s = Crafty.e("Snake").snake(blocksize, 150, 150);
            Crafty.e("Floor").floor(blocksize);
            s.Parts[0].addComponent("Player").player(blocksize);
            return this;
        }
    })

    var g = Crafty.e("SnakeGame").snakegame(16);

});?